## Install and Setup

1. git clone git@bitbucket.org:spaceai/nrep.git
1. sh ~/nrep/setup.sh

## Test using Fake Groundstation

1. source ~/nrep/fake.sh
1. http://192.168.x.z:5000

## Boot Script for "The Real Thing"

1. source ~/nrep/payload.sh
1. Bob's your uncle

## Overview

payload.py is the main script, in turn it imports functions from scripts named read_*.py

This script runs four threads:

1. Read lines from Serial Port and Enqueue
1. Dequeue lines, if [ESC] in sequence discard previous data and clear Response Queue, execute read_* command and enqueue Response
	* If command is X (execute), put it into Async Execute Queue
1. Dequeue X commands from Async Queue, execute them and Enqueue results in Response Queue
1. Dequeue responses and send to Serial Port

## Supported Commands (Implemented and Stubs)

* S (Status request) - current response is random numbers (no sensors connected yet)
* N (28 VDC on) - stub - not implemented yet
* O (28 VDC off) - stub - not implemented yet
* T (Transmit file from Payload) - reads files in Binary mode, encondes it using Intel HEX and enqueues the lines in Response Queue
* R (Receive file to Payload) - reads HEX lines from Serial Queue until EOF and saves it in Binary format
* X (Execute) - asynchronously runs shell command (_beware potentially dangerous commands!!_)
* A (Abort) - clears Async Queue
* D (Streaming mode) - stub - not implemented yet
* W (Warmboot) - waits a minute and reboots OS (sudo shutdown -r now)