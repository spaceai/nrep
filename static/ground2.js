(() => {
    // setup socket
    const namespace = '';   // '/fake';
    const socket = io(location.protocol +'//'+ document.domain +':'+ location.port + namespace);
    socket.connect();
    // receive
    socket.on('status', function(msg) {
        $('#status').html(msg)
        return 'Status ack'
    })
    socket.on('response', function(msg) {
        $('#response').prepend(`<li>${msg}</li>`)
    })

    // send
    $('.btn[name*="cmd_"]').click(function(event) {
        command = event.target.name.substr(4)

        if (command=='T') {
            file = $('input[name="file"]').val()
            if (! file) {
                return false;
            }
            command += file //  append file name
        }
        if (command=='R') {
            file = $('input[type="file"]')[0].files[0].name
            if (! file) {
                return false;
            }
            command += file //  append file name
        }
        socket.emit('command', command, function(ack) {
            console.log('command ack', ack)
        });
        return false;
    });

    // Interval function that tests message latency by sending a "ping" message.
    var ping_pong_times = [];
    var start_time;
    window.setInterval(function() {
        start_time = (new Date).getTime();
        socket.emit('pingy', start_time, function(ack) {
            console.log('pinging ack', ack)
        });
    }, 10000);

    // Handler for the "pong" message.
    socket.on('pongy', function(msg) {
        console.log('pinged from', msg);
        var latency = (new Date).getTime() - start_time;
        ping_pong_times.push(latency);
        ping_pong_times = ping_pong_times.slice(-30); // keep last 30 samples
        var sum = 0;
        for (var i = 0; i < ping_pong_times.length; i++)
            sum += ping_pong_times[i];
        $('#ping-pong').text(Math.round(10 * sum / ping_pong_times.length) / 10);
    });
})();
