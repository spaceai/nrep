#!/usr/bin/env bash

# invoke this script like:
# source ~/nrep/fake.sh
#
cd ~/nrep
source ./env/bin/activate
export FLASK_APP=ground2.py
flask run --host=0.0.0.0
