from queue import Queue
import random
import shlex
import subprocess
import time

xq = Queue()  # execute queue (local) (could be tied to this thread)

def read_abort(stub, pq):
    #
    # Page 36:
    # If the payload receives an Abort command (11.3.1.9) while executing an Execute command asynchronously, 
    # it may, but is not required to, abort that execution.
    #
    # with xq.mutex:
    xq.queue.clear()
    print('Aborted - Queue size now is '+ str(xq.qsize()))
    #
    # If no asynchronous operation was pending (any more), the payload may simply respond OK.
    #
    return 'OK'

def read_execute(cmd, pq):
    print('Queuing '+ cmd)
    xq.put_nowait(cmd)
    print('Queue size is '+ str(xq.qsize()))
    return 'OK'

def read_execute_async(rq):
    while True:
        if not xq.empty():
            cmd = xq.get_nowait()
            print('Executing '+ cmd)

            result = subprocess.run(shlex.split(cmd), stdout=subprocess.PIPE, timeout=4)
            r = result.stdout.decode('utf-8')
            # print('Async execute:\n'+ r)
            rq.put_nowait(r)
            xq.task_done()
        
        else:
            time.sleep(0.1)
