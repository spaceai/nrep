#
#   Fake groundstation console
#
from flask import Flask, render_template
from flask_socketio import SocketIO, emit, disconnect

app = Flask(__name__)
app.config['SECRET_KEY'] = 'H3ll0 155!'

async_mode = 'gevent'
socketio = SocketIO(app, async_mode=async_mode)

@app.route('/')
def index():
    return render_template('ground2.html', async_mode=socketio.async_mode)

@socketio.on('pingy')
def pingPong(what):
    # print('Got a ping: '+ str(what))
    socketio.emit('pongy', what)
    return 'Acknowledged'


def response(data):
    print('> '+ data)
    socketio.emit('response', data)

#
#   Fake serial comm
#
import serial 

import ground1
import payload

# print(dir(ground1))
# print(dir(payload))

# 4.2.3.1.1CDC Abstract Control Model (Serial Communication)
# catch unavailability error: SerialException
try:
    usb1 = serial.serial_for_url('loop://', timeout=5)
    usb2 = serial.serial_for_url('loop://', timeout=5)

    payload.thread_start(usb1, usb2)
    ground1.thread_start(usb1, usb2, response)
    socketio.emit('response', 'Threads started')

except serial.SerialException:
    socketio.emit('response', 'no carrier <g>')


@socketio.on('command')
def command(data):
    ground1.command(data)
    
    if data.startswith('R'):
        file = 'server/'+ data[1:]
        print('Should transmit:', file)
        try:
            lines = [line.rstrip('\n') for line in open(file)]
        except:
            print('Error: file not found in ground server')
            return
        
        for line in lines:
            ground1.content(line)
        
        print('File sent.')
