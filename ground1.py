from queue import Queue
cq = Queue()    # command queue

import serial 
import threading
import time

def command(data):
    cs = b'\x1B'+ bytearray(data, 'utf-8') + b'\x0A'
    cq.put_nowait(cs)

def content(line):
    cs = bytearray(line, 'utf-8') + b'\x0A'
    cq.put_nowait(cs)

def status_to_queue():
    while True:
        if cq.empty():
            command('S{:.6f}'.format(time.time()))
        
        time.sleep(10)   # Status will be called every 1 second instead!

def write_to_port(s):
    while s.is_open:
        if not cq.empty():
            try:
                s.write(cq.get_nowait())
                cq.task_done()
            
            except serial.SerialTimeoutException:
                print('Write timeout, keep moving')
        
        else:
            time.sleep(0.5)

def read_from_port(s, pr):
    while True:
        if (s.in_waiting > 0):
            line = s.readline().decode().strip()
            pr(line)
        
        else:
            time.sleep(0.5)

def thread_start(sw, sr, pr):
    thread = threading.Thread(target=status_to_queue, args=())
    thread.start()

    thread = threading.Thread(target=write_to_port, args=(sw,))
    thread.start()

    thread = threading.Thread(target=read_from_port, args=(sr,pr))
    thread.start()
