import random

# Page 37:
# 4.2.3.3 Health and Status Monitoring
# The serial communication channel is used periodically by the NREP DHS to query internal payloads for their internal status, 
# by sending the Status command, at a nominal rate of once per second 
# (except during the execution of other commands).
#
def read_status(ts, pq):
    temp1 = str(random.randint(-5, 5))
    temp2 = str(random.randint(10, 25))
    curr1 = str(random.randint(1000, 2000))
    curr2 = ''
    scode = '0'
    stext = ts
    return ':'.join(('HloNewmn', temp1, temp2, curr1, curr2, scode, stext))
