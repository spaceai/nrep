from queue import Queue

pq = Queue()    # process queue
rq = Queue()    # response queue
sq = Queue()    # status message queue

import serial
import threading
import time

# GROUNDSTATION
# READ commands
# https://stackoverflow.com/questions/9168340/using-a-dictionary-to-select-function-to-execute/9168387#9168387
#
from read_power_on_off import *
from read_recv_xmit_file import *
from read_status import *
from read_execute import *

def read_data_streaming(sink, pq):
    print('Sinking '+ sink)
    return 'NOK:'+ 'USB Network Interface unsupported yet'

def read_warmboot(stub, pq):
    time.sleep(1)
    return read_execute('sudo shutdown -r now')

read_dict = {
    'N': read_power_on, 'O': read_power_off, 
    'R': read_receive_file, 'T': read_transmit_file,
    'S': read_status, 
    'X': read_execute, 'A': read_abort, 
    'D': read_data_streaming, 
    'W': read_warmboot
}

def read_function(f):
    print('Read function:', end=' ')
    print(f)
    if 'cmd' in f:
        print('Command received: '+ f['cmd'][0:1])
    if 'data' in f:
        print('Data received: '+ f['data'])

    if False and f['cmd'][0:1]=='R':
        # (R)eceive file command, needs extra reads
        # TODO: what if I get an ESC right here?
        r = read_receive_file(c, pq)
    else:
        r = read_dict.get(f['cmd'][0:1], lambda: 'Invalid')(f['cmd'][1:], pq)
    
    rq.put_nowait(r)
#
# THREADED
#
def read_from_port(s):
    while True:
        while s.in_waiting:
            line = s.readline()
            esc = line.find(b'\x1B')
            if esc > -1:
                if esc > 0:
                    print(line[0:esc], end='')
                    print(' discarded')
                
                pq.queue.clear()
                pq.put_nowait({ 'cmd': line[esc+1:-1].decode() })  # command
            else:
                pq.put_nowait({ 'data': line.decode() })      # maybe hex file

            time.sleep(0.1)
    
    print('Serial closed')

def process_queue(s):
    while True:
        if not pq.empty():
            f = pq.get_nowait()
            read_function(f)
            pq.task_done()
        else:
            time.sleep(0.1)

def write_to_port(s):
    while True:
        if not rq.empty():
            r = rq.get_nowait()
            s.write(bytearray(r, 'utf-8') + b'\x0A')
            rq.task_done()
        else:
            time.sleep(0.1)

def thread_start(sr, sw):
    thread1 = threading.Thread(name='read_from_port', target=read_from_port, args=(sr,))
    # thread1.daemon = True
    thread1.start()

    thread2 = threading.Thread(name='process_queue', target=process_queue, args=(sr,))
    thread2.start()

    thread3 = threading.Thread(name='write_to_port', target=write_to_port, args=(sw,))   # watch out! serial write
    thread3.start()

    thread4 = threading.Thread(name='read_execute_async', target=read_execute_async, args=(rq,))
    thread4.start()

    if threading.main_thread()==thread1:
        print ('this is it')

    thread1.join()

if __name__ == '__main__':
    print('payload called as __main__')
    s = serial.Serial('/dev/ttyS0', 57600, timeout=5)
    thread_start(s, s)
