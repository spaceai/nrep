from intelhex import IntelHex
import time

ih = IntelHex()

def wait_for_queue(queue, strikes, delay):
    while queue.empty() and strikes > 0:
        time.sleep(delay)
        strikes -= 1
    
    return strikes

def read_receive_file(file, dq):
    print(wait_for_queue(dq, 5, 0.9))

    if dq.empty():
        return 'NOK:'+ file +' strike out'
    
    # write to temp file BEGIN
    fh = open('temp.hex', 'w')
    c = dq.get_nowait()
    dq.task_done()
    while c['data']!=':00000001FF':
        fh.write(c['data'])

        if wait_for_queue(dq, 5, 0.9) > 0:
            c = dq.get_nowait()
            dq.task_done()
        else:
            break

    fh.write(':00000001FF')
    fh.close()
    # write to temp file END

    try:
        ih.fromfile('temp.hex', format='hex')
        ih.tofile(file, format='bin')

    except:
        return 'NOK:'+ file +' failed to convert from Intel HEX'
    
    return 'OK:'+ file +' written (theoretically)'

def read_transmit_file(file, pq):
    try:
        ih.fromfile(file, format='bin')
        ih.write_hex_file('temp.hex')
        with open('temp.hex', 'r') as fh:
            data = fh.read()
            fh.close()
            return data +'OK'

    except FileNotFoundError:
        return 'NOK:'+ file +' - file not found'
